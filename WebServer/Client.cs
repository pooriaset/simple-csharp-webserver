﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace WebServer
{
    class Client
    {
        private readonly NetworkStream _networkStream;
        private readonly MemoryStream _memoryStream = new MemoryStream();
        private readonly StreamReader _streamReader;
        private const string ServerName = "Pooria Web Server V0.1";

        public Client(Socket socket)
        {
            _networkStream = new NetworkStream(socket, true);
            _streamReader = new StreamReader(_memoryStream);
        }

        public async void Do()
        {
            var buffer = new byte[4096];
            while (true)
            {
                var bytesRead = await _networkStream.ReadAsync(buffer, 0, buffer.Length);

                if (bytesRead == 0)
                    return;


                _memoryStream.Seek(0, SeekOrigin.End);
                _memoryStream.Write(buffer, 0, bytesRead);
                var done = ProcessHeader();

                if (done)
                    break;
            }

        }

        private bool ProcessHeader()
        {
            while (true)
            {
                _memoryStream.Seek(0, SeekOrigin.Begin);
                var line = _streamReader.ReadLine();
                if (line == null)
                    break;

                if (!line.ToUpperInvariant().StartsWith("GET ")) continue;

                var path = line.Split(' ')[1].TrimStart('/');
                path = path.Split('?')[0];

                if (!path.Contains("."))
                {
                    path += "index.html";
                }

                SendFile("root/" + path);
                return true;

            }

            return false;
        }

        private async void SendFile(string path)
        {
            byte[] data;
            string responseCode;
            string contentType;
            try
            {
                if (File.Exists(path))
                {
                    // Read file
                    data = File.ReadAllBytes(path);
                    contentType = GetContentType(Path.GetExtension(path).TrimStart(".".ToCharArray()));
                    responseCode = "200 OK";
                }
                else
                {
                    data = Encoding.ASCII.GetBytes("<html><body><h1>404 File Not Found</h1></body></html>");
                    contentType = "text/html";
                    responseCode = "404 Not found";
                }
            }
            catch (Exception exception)
            {
                data = Encoding.ASCII.GetBytes("<html><body><h1>500 Internal server error</h1><pre>" + exception + "</pre></body></html>");
                contentType = "text/html";
                responseCode = "500 Internal server error";
            }

            var header = $"HTTP/1.1 {responseCode}\r\n" + $"Server: {ServerName}\r\n" +
                            $"Content-Length: {data.Length}\r\n" + $"Content-Type: {contentType}\r\n" +
                            "Keep-Alive: Close\r\n" + "\r\n";


            var headerBytes = Encoding.ASCII.GetBytes(header);
            await _networkStream.WriteAsync(headerBytes, 0, headerBytes.Length);
            await _networkStream.WriteAsync(data, 0, data.Length);
            await _networkStream.FlushAsync();

            _networkStream.Dispose();
        }

        private static string GetContentType(string extension)
        {
            if (Regex.IsMatch(extension, "^[a-z0-9]+$", RegexOptions.IgnoreCase | RegexOptions.Compiled))
                return (Registry.GetValue(@"HKEY_CLASSES_ROOT\." + extension, "Content Type", null) as string)
                    ?? "application/octet-stream";

            return "application/octet-stream";
        }
    }
}
