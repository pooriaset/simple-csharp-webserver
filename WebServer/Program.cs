﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServer
{
    class Program
    {
        private static Listener _listener;
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Mady By Pooria Setayesh");
            Console.ForegroundColor = ConsoleColor.White;

            _listener = new Listener(9090);

            Console.WriteLine("Listening on Port 9090 : ");
            Console.WriteLine("Press any key to exit!");
            Console.ReadKey();
        }
    }
}
