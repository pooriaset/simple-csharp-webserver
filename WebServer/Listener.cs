﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace WebServer
{
    class Listener
    {
        public readonly int Port;
        private readonly TcpListener _tcpListener;

        public Listener(int port)
        {
            Port = port;

            _tcpListener = new TcpListener(IPAddress.Any, Port);
            _tcpListener.Start();

            Task.Factory.StartNew(ListenLoop);

        }

        private async void ListenLoop()
        {
            while (true)
            {
                var socket = await _tcpListener.AcceptSocketAsync();
                if (socket == null)
                    break;

                var client = new Client(socket);
                await Task.Factory.StartNew(client.Do);
            }
        }
    }
}
